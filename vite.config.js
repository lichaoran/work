import { defineConfig } from "vite";

export default defineConfig({
  build: {
    rollupOptions: {
      input: {
        app: "./packages/app/main.js", // app 子项目入口
        library: "./packages/library/index.js", // library 子项目入口
      },
      output: {
        entryFileNames: "[name].js", // 输出文件名
        chunkFileNames: "chunks/[name].js", // 指定 chunk 的输出路径
        format: "es", // 输出格式（ES Module）
      },
    },
  },
});
