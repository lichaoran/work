'use strict'

const pcAtom = require('..')
const assert = require('assert').strict

assert.strictEqual(pcAtom(), 'Hello from pcAtom')
console.info('pcAtom tests passed')
